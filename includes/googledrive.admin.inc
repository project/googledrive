<?php

function googledrive_admin_settings() {
  $form = array();
  $private_path = variable_get('file_private_path');

  if (empty($private_path)) {
    drupal_set_message(t('Private file system path is not configured. To configure , click !here.', array('!here' => l('here', 'admin/config/media/file-system'))), 'error');
  }

  $form['googledrive_client_creds'] = array(
    '#title' => t('Client Credentials'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('googledrive_client_creds'),
    '#upload_location' => 'private://googledrive',
    '#description' => t('Upload client credentials (JSON format) downloaded from Google service.'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('json'),
    ),
  );

  $form['googledrive_appname'] = array(
    '#title' => t('Application Name'),
    '#type' => 'textfield',
    '#default_value' => variable_get('googledrive_appname'),
  );

  $form['googledrive_debug_mode'] = array(
    '#title' => t('Debug mode'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('googledrive_debug_mode'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value'  => t('Save configuration'),
    '#submit' => array('googledrive_admin_settings_submit'),
  );

  return $form;
}

function googledrive_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']['googledrive_client_creds'])) {
    if ($form_state['values']['googledrive_client_creds'] == 0) {
      $document_exist_id = variable_get('googledrive_client_creds');
      $document_exist_file = file_load($document_exist_id);
      if (!empty($document_exist_file)) {
        file_usage_delete($document_exist_file, 'googledrive', 'client_credentials', $document_exist_id);

        file_delete($document_exist_file, TRUE);
        db_delete('file_managed')
          ->condition('fid', $document_exist_id)
          ->execute();

        variable_set('googledrive_client_creds', '');
      }
    }
    else {
      $current_document = file_load($form_state['values']['googledrive_client_creds']);
      if (!empty($current_document->fid)) {
        $file_id = $current_document->fid;
        variable_set('googledrive_client_creds', $file_id);
        file_usage_add($current_document, 'googledrive', 'client_credentials', $file_id);
        $current_document->status = FILE_STATUS_PERMANENT;
        file_save($current_document);
      }
    }
  }

  variable_set('googledrive_debug_mode', $form_state['values']['googledrive_debug_mode']);
  variable_set('googledrive_appname', $form_state['values']['googledrive_appname']);

  drupal_set_message(t('The configuration has been saved successfully.'));
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing users.
 */
function googledrive_user_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('users')->fields('users', array('uid','name'))->condition('name', db_like($string) . '%', 'LIKE')->range(0, 10)->execute();
    foreach ($result as $user) {
      $userobj = user_load($user->uid);
      if (user_access('view googledrive file', $userobj)) {
        $matches[$user->name] = check_plain($user->name);
      }
    }
  }

  drupal_json_output($matches);
}