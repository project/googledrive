<?php

/**
 * List users page.
 *
 * @return array
 */
function googledrive_gfile_users_page($googledrive_file) {
  $googledrive_gusers = entity_load('googledrive_guser', false, array('googledrive_gfile_id' => $googledrive_file->id), true);

  $headers = array(t('Name'), t('Actions'));
  $rows = array();

  foreach ($googledrive_gusers as $id => $googledrive_guser) {
    $user = user_load($googledrive_guser->uid);
    $links_array = array(
      l(t('edit'), 'admin/structure/googledrive-files/' . $googledrive_guser->googledrive_gfile_id . '/users/' . $googledrive_guser->id . '/edit'),
      l(t('delete'), 'admin/structure/googledrive-files/' . $googledrive_guser->googledrive_gfile_id . '/users/' . $googledrive_guser->id . '/delete'),
    );

    $links = implode('&nbsp;', $links_array);
    $data = l($user->name, 'admin/structure/googledrive-files/' . $googledrive_guser->googledrive_gfile_id . '/users/' . $id);
    $rows[] = array($data, $links);
  }

  $output = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#empty' => t('No users available.'),
  );

  return $output;
}

/**
 * Drive User form
 */
function googledrive_guser_form($form, &$form_state, $googledrive_gfile = array(), $googledrive_guser = array(), $op = '') {
  if (isset($googledrive_guser->uid) && is_numeric($googledrive_guser->uid))
    $user = user_load($googledrive_guser->uid);

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Select User'),
    '#description' => t('Search user by username.'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'googledrive-user/autocomplete',
    '#default_value' => isset($user->name) ? $user->name : '',
    '#weight' => -1,
  );

  $form['email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Email Address'),
    '#description' => t('Email address associated with Google.'),
    '#maxlength' => 60,
    '#default_value' => isset($googledrive_guser->email_address) ? $googledrive_guser->email_address : '',
  );

  $form['use_email_address'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use above email address'),
    '#default_value' => isset($googledrive_guser->use_email_address) ? $googledrive_guser->use_email_address : 0,
  );

  $form['access_granted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Grant access to file on Google Drive?'),
    '#default_value' => isset($googledrive_guser->access_granted) ? $googledrive_guser->access_granted : 0,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Status'),
    '#default_value' => isset($googledrive_guser->status) ? $googledrive_guser->status : 0,
  );

  $form['googledrive_gfile_id'] = array(
    '#type' => 'hidden',
    '#value' => isset($googledrive_gfile->id) ? $googledrive_gfile->id : '',
  );

  if ($op == 'edit') {
    $form['entity_edit'] = array(
      '#type' => 'hidden',
      '#value' => TRUE,
    );

    $form['permission_id'] = array(
      '#type' => 'hidden',
      '#value' => isset($googledrive_guser->permission_id) ? $googledrive_guser->permission_id : '',
    );

    $form['entity_id'] = array(
      '#type' => 'hidden',
      '#value' => isset($googledrive_guser->id) ? $googledrive_guser->id : '',
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/googledrive-files/' . $googledrive_gfile->id . '/users'),
  );

  return $form;
}

/**
 * Drive User form validate.
 *
 * @param $form
 * @param $form_state
 */
function googledrive_guser_form_validate($form, &$form_state) {}

/**
 * Drive user form submit.
 *
 * @param $form
 * @param $form_state
 * @throws Exception
 */
function googledrive_guser_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $time = time();

  $user = user_load_by_name($values['name']);

  if ($values['status']) {
    $client = googledrive_get_client();
    $service = new Google_Service_Drive($client);
    $googledrive_file = googledrive_gfile_load($values['googledrive_gfile_id']);
    $googledrive_file_id = $googledrive_file->file_id;
    if (empty($googledrive_file_id) && googledrive_is_debugmode()) {
      watchdog('googledrive', t('File ID missing. Please configure proper Google Drive File ID. !values', array('!values' => print_R($googledrive_file, true))));
    }

    if ($values['use_email_address']) {
      $email_address = $values['email_address'];
    }
    else {
      $email_address = $user->mail;
    }

    if ($values['access_granted']) {
      $permission = new Google_Service_Drive_Permission();
      $permission->setEmailAddress($email_address);
      $permission->setRole('reader');
      $permission->setType('user');

      try {
        $options['sendNotificationEmail'] = FALSE;
        $file_permission = $service->permissions->create($googledrive_file_id, $permission, $options);
        $values['permission_id'] = $file_permission->id;
        if (googledrive_is_debugmode()) {
          watchdog('googledrive', t('Permission granted to <em>!usern</em> for file <em>!filen</em>', array('!usern' => $email_address, '!filen' => $googledrive_file->title)));
        }
      }
      catch (Google_Service_Exception $e) {
        if (googledrive_is_debugmode()) {
          watchdog('googledrive', t('Permission could not be assigned. !exc_info', array('!exc_info' => $e)));
        }
      }
    }
    else {
      if (!empty($values['permission_id'])) {
        try {
          $file_permission = $service->permissions->delete($googledrive_file_id, $values['permission_id']);
          if (googledrive_is_debugmode()) {
            watchdog('googledrive', t('Permission revoked from <em>!usern</em> for file <em>!filen</em>', array('!usern' => $email_address, '!filen' => $googledrive_file->title)));
          }
        }
        catch (Google_Service_Exception $e) {
          if (googledrive_is_debugmode()) {
            watchdog('googledrive', t('Permission not found. !exc_info', array('!exc_info' => $e)));
          }
        }

        $values['permission_id'] = '';
      }
      else {
        if (googledrive_is_debugmode()) {
          watchdog('googledrive', t('Permission ID is missing. !values', array('!values' => print_R($googledrive_file, true))));
        }
      }
    }

    if (empty($file_permission)) {
      if (googledrive_is_debugmode()) {
        watchdog('googledrive', t('Sorry, couldn\'t grant access to a file. !values', array('!values' => print_R($googledrive_file, true))));
      }
    }
  }

  if (isset($values['entity_edit']) && !$values['entity_edit']) {
    $googledrive_guser = googledrive_guser_new(); //an object for the entity training_program is created
    $googledrive_guser->created = $time;
    $googledrive_guser->updated = $time;

  }
  else {
    $googledrive_guser = array();
    if (!empty($values['entity_id'])) {
      $googledrive_guser = googledrive_guser_load($values['entity_id']);
    }

    if (empty($googledrive_guser)) {
      $googledrive_guser = googledrive_guser_new();
      $googledrive_guser->created = $time;
    }

    $googledrive_guser->updated = $time;
  }

  $googledrive_guser->uid = $user->uid;
  $googledrive_guser->use_email_address = $values['use_email_address'];
  $googledrive_guser->email_address = $values['email_address'];
  $googledrive_guser->googledrive_gfile_id = $values['googledrive_gfile_id'];
  if (!empty($values['permission_id'])) {
    $googledrive_guser->permission_id = $values['permission_id'];
    $googledrive_guser->access_granted = $values['access_granted'];
  }
  $googledrive_guser->status = $values['status'];

  entity_save('googledrive_guser', $googledrive_guser);//save the entity
  drupal_set_message(t('User saved successfully.'));
  drupal_goto('admin/structure/googledrive-files/' . $values['googledrive_gfile_id'] . '/users');
}

/**
 * Google Drive file entity display page.
 */
function googledrive_guser_page($googledrive_gfile, $googledrive_guser) {
  global $base_url;
  $user = user_load($googledrive_guser->uid);
  // Again, load and add all your standard entity data, then call
  // field_atttach_prepare_view somewhere to add the field_display
  $data['name'] = check_plain($user->name);
  $data['mail'] = check_plain($user->mail);
  $data['email_address'] = check_plain($googledrive_guser->email_address);
  $data['use_email_address'] = $googledrive_guser->use_email_address ? t('Yes') : t('No');
  $data['access_granted'] = $googledrive_guser->access_granted ? t('Yes') : t('No');
  $data['status'] = $googledrive_guser->status ? t('Enabled') : t('Disabled');
  $data['back_link'] = $base_url . '/admin/structure/googledrive-files/' . $googledrive_gfile->id . '/users';

  return theme('googledrive_guser', array('data' => $data));
}



/**
 * Drive user delete form.
 */
function googledrive_guser_delete_form($form, &$form_state, $googledrive_gfile = array(), $googledrive_guser = array()) {
  $question = t('Are you sure you want to delete the Google Drive user?');

  $path = '/admin/structure/googledrive-files/' . $googledrive_gfile->id . '/users';

  $form['entity_id'] = array(
    '#type' => 'hidden',
    '#value' => $googledrive_guser->id,
  );

  $form['googledrive_gfile_id'] = array(
    '#type' => 'hidden',
    '#value' => $googledrive_gfile->id,
  );

  $form = confirm_form($form, $question, $path);

  return $form;
}

/**
 * Drive user delete submit.
 *
 * @param $form
 * @param $form_state
 * @throws Exception
 */
function googledrive_guser_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (!is_numeric($values['entity_id'])) {
    drupal_goto('admin/structure/googledrive-files');
  }

  $googledrive_user = googledrive_guser_load($values['entity_id']);

  $client = googledrive_get_client();
  $service = new Google_Service_Drive($client);
  $googledrive_file = googledrive_gfile_load($values['googledrive_gfile_id']);
  $googledrive_file_id = $googledrive_file->file_id;
  if (empty($googledrive_file_id) && googledrive_is_debugmode()) {
    watchdog('googledrive', t('File ID missing. Please configure proper Google Drive File ID. !values', array('!values' => print_R($googledrive_file, true))));
  }

  if (!empty($googledrive_user->permission_id)) {
    $file_permission = $service->permissions->delete($googledrive_file_id, $googledrive_user->permission_id);
  }
  else {
    if (googledrive_is_debugmode()) {
      watchdog('googledrive', t('Permission ID is missing. !values', array('!values' => print_R($googledrive_file, true))));
    }
  }

  if (empty($file_permission) && googledrive_is_debugmode()) {
    watchdog('name', t('Sorry, couldn\'t grant access to a file. !values', array('!values' => print_R($googledrive_file, true))));
  }

  db_delete('googledrive_gusers')
    ->condition('id', $values['entity_id'])
    ->execute();

  drupal_set_message(t('Google Drive file has been deleted successfully.'));
  drupal_goto('admin/structure/googledrive-files/' . $values['googledrive_gfile_id'] . '/users');
}
