<?php

/**
 * List all Drive files.
 */
function googledrive_list() {
  $googledrive_gfiles = entity_load('googledrive_gfile', false, array(), true);
  $headers = array(t('Name'), t('Actions'));
  $rows = array();
  foreach ($googledrive_gfiles as $id => $googledrive_gfile) {
    $links_array = array(
      l(t('edit'), 'admin/structure/googledrive-files/' . $id . '/edit'),
      l(t('delete'), 'admin/structure/googledrive-files/' . $id . '/delete'),
      l('manage users', 'admin/structure/googledrive-files/' . $id . '/users'),
    );

    $links = implode('&nbsp;', $links_array);
    $data = l($googledrive_gfile->title, 'admin/structure/googledrive-files/' . $id);
    $rows[] = array($data, $links);
  }

  $output = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#empty' => t('No files available.'),
  );

  return $output;
}

/**
 * Drive File form.
 */
function googledrive_gfile_form($form, &$form_state, $googledrive_gfile = array(), $op = '') {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($googledrive_gfile->title) ? $googledrive_gfile->title : '',
  );

  $file_types = array(
    'pdf' => 'PDF',
    'doc' => 'DOC',
    'ppt' => 'PPT',
    'xls' => 'XLS',
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('File Type'),
    '#options' => $file_types,
    '#default_value' => isset($googledrive_gfile->type) ? $googledrive_gfile->type : '',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($googledrive_gfile->description) ? $googledrive_gfile->description : '',
  );

  $form['file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('File path on Google Drive'),
    '#default_value' => isset($googledrive_gfile->file_path) ? $googledrive_gfile->file_path : '',
  );

  $form['file_id'] = array(
    '#type' => 'textfield',
    '#title' => t('File ID on Google Drive'),
    '#default_value' => isset($googledrive_gfile->file_id) ? $googledrive_gfile->file_id : '',
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Status'),
    '#default_value' => isset($googledrive_gfile->status) ? $googledrive_gfile->status : 0,
  );

  if ($op == 'edit') {
    $form['entity_edit'] = array(
      '#type' => 'hidden',
      '#value' => TRUE,
    );

    $form['entity_id'] = array(
      '#type' => 'hidden',
      '#value' => $googledrive_gfile->id,
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/googledrive-files'),
  );

  return $form;
}

/**
 * File form submit.
 *
 * @param $form
 * @param $form_state
 */
function googledrive_gfile_form_submit($form, &$form_state) {
  global $user;
  $values = $form_state['values'];
  $time = time();

  if (!$values['entity_edit']) {
    $googledrive_gfile = googledrive_gfile_new(); //an object for the entity training_program is created
    $googledrive_gfile->created = $time;
    $googledrive_gfile->updated = $time;

  }
  else {
    $googledrive_gfile = googledrive_gfile_load($values['entity_id']);

    if (empty($googledrive_gfile)) {
      $googledrive_gfile = googledrive_gfile_new();
      $googledrive_gfile->created = $time;
    }

    $googledrive_gfile->updated = $time;
  }

  $googledrive_gfile->title = $values['title'];
  $googledrive_gfile->uid = $user->uid;
  $googledrive_gfile->type = $values['type'];
  $googledrive_gfile->description = $values['description'];
  $googledrive_gfile->file_path = $values['file_path'];
  $googledrive_gfile->file_id = $values['file_id'];
  $googledrive_gfile->status = $values['status'];

  entity_save('googledrive_gfile', $googledrive_gfile);//save the entity
  drupal_set_message(t('File saved successfully.'));
  drupal_goto('admin/structure/googledrive-files');
}

/**
 * Google Drive file entity display page.
 */
function googledrive_gfile_page($googledrive_gfile) {
  global $base_url;
  // Again, load and add all your standard entity data, then call
  // field_atttach_prepare_view somewhere to add the field_display
  $data['title'] = check_plain($googledrive_gfile->title);
  $data['type'] = $googledrive_gfile->type;
  $data['description'] = check_plain($googledrive_gfile->description);
  $data['file_path'] = check_plain($googledrive_gfile->file_path);
  $data['file_id'] = check_plain($googledrive_gfile->file_id);
  $data['status'] = $googledrive_gfile->status ? t('Enabled') : t('Disabled');
  $data['back_link'] = $base_url . '/admin/structure/googledrive-files';

  return theme('googledrive_gfile', array('data' => $data));
}

/**
 * File delete form.
 */
function googledrive_gfile_delete_form($form, &$form_state, $googledrive_gfile = array()) {
  $question = t('Are you sure you want to delete the Google Drive file?');

  $path = '/admin/structure/googledrive-files';

  $form['entity_id'] = array(
    '#type' => 'hidden',
    '#value' => $googledrive_gfile->id,
  );

  $form = confirm_form($form, $question, $path);

  return $form;
}

/**
 * File delete form submit.
 *
 * @param $form
 * @param $form_state
 */
function googledrive_gfile_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  db_delete('googledrive_gfiles')
    ->condition('id', $values['entity_id'])
    ->execute();

  drupal_set_message(t('Google Drive file has been deleted successfully.'));
  drupal_goto('admin/structure/googledrive-files');
}