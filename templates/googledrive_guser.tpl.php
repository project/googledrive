<style>
  .googledrive-file-details tr td {
    border: 1px solid;
  }
</style>

<table class="googledrive-file-details">
  <tr>
    <td>Username</td>
    <td><?php print($data['name']); ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><?php print($data['mail']); ?></td>
  </tr>
  <tr>
    <td>Google Email Address</td>
    <td><?php print($data['email_address']); ?></td>
  </tr>
  <tr>
    <td>Use Google Email Address</td>
    <td><?php print($data['use_email_address']); ?></td>
  </tr>
  <tr>
    <td>Access Granted</td>
    <td><?php print($data['access_granted']); ?></td>
  </tr>
  <tr>
    <td>Status</td>
    <td><?php print($data['status']); ?></td>
  </tr>
</table>
<div><a href="<?php print($data['back_link']); ?>">Back</a></div>