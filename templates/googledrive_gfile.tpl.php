<style>
  .googledrive-file-details tr td {
    border: 1px solid;
  }
</style>

<table class="googledrive-file-details">
  <tr>
    <td>Title</td>
    <td><?php print($data['title']); ?></td>
  </tr>
  <tr>
    <td>Type</td>
    <td><?php print($data['type']); ?></td>
  </tr>
  <tr>
    <td>Description</td>
    <td><?php print($data['description']); ?></td>
  </tr>
  <tr>
    <td>File Path</td>
    <td><?php print($data['file_path']); ?></td>
  </tr>
  <tr>
    <td>File ID</td>
    <td><?php print($data['file_id']); ?></td>
  </tr>
  <tr>
    <td>Status</td>
    <td><?php print($data['status']); ?></td>
  </tr>
</table>
<div><a href="<?php print($data['back_link']); ?>">Back</a></div>